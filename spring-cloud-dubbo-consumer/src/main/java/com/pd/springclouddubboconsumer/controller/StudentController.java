package com.pd.springclouddubboconsumer.controller;

import com.pd.springclouddubboapi.dto.resp.StudentDto;
import com.pd.springclouddubboapi.service.StudentQueryServiceI;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @PACKAGE_NAME: com.pd.springclouddubboconsumer.controller
 * @AUTH: chendengqian
 * @DATETIME: 2022/12/1 21:05
 * @PROJECT_NAME: spring-cloud-dubbo-202104
 **/
@RestController
@RequestMapping("/test")
@Slf4j
public class StudentController {
    @DubboReference(group = "mc",version = "1.0.01",check = true)
    private StudentQueryServiceI studentQueryServiceI;

    @GetMapping("/student")
    public StudentDto getStu() {
        StudentDto studentDto = studentQueryServiceI.queryStudent(null);
        log.info("consumer resp:" + studentDto);
        return studentDto;

    }
}
