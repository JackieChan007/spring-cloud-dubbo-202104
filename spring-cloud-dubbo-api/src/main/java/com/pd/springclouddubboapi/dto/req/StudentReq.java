package com.pd.springclouddubboapi.dto.req;

import lombok.Data;

import java.io.Serializable;

/**
 * TODO
 *
 * @PACKAGE_NAME: com.pd.springclouddubboapi.dto.req
 * @AUTH: chendengqian
 * @DATETIME: 2022/12/1 20:57
 * @PROJECT_NAME: spring-cloud-dubbo-202104
 **/
@Data
public class StudentReq implements Serializable {
    private String name;
}
