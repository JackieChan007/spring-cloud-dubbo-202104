package com.pd.springclouddubboapi.dto.resp;

import lombok.Data;

import java.io.Serializable;

/**
 * TODO
 *
 * @PACKAGE_NAME: com.pd.springclouddubboapi.dto.req
 * @AUTH: chendengqian
 * @DATETIME: 2022/12/1 20:54
 * @PROJECT_NAME: spring-cloud-dubbo-202104
 **/
@Data
public class StudentDto implements Serializable {
    private String name;
    private String addr;
    private Integer age;
}
