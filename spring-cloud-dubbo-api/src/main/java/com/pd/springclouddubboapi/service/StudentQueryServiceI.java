package com.pd.springclouddubboapi.service;

import com.pd.springclouddubboapi.dto.req.StudentReq;
import com.pd.springclouddubboapi.dto.resp.StudentDto;

/**
 * TODO
 *
 * @PACKAGE_NAME: com.pd.springclouddubboapi.service
 * @AUTH: chendengqian
 * @DATETIME: 2022/12/1 20:58
 * @PROJECT_NAME: spring-cloud-dubbo-202104
 **/
public interface StudentQueryServiceI {
    /**
     * 查询学生成绩
     *
     * @param studentReq
     * @return
     */
    public StudentDto queryStudent(StudentReq studentReq);

    public void sayHello();
}
