package com.pd.springclouddubboshenyuprovider.controller;

import com.pd.springclouddubboapi.dto.req.StudentReq;
import com.pd.springclouddubboapi.dto.resp.StudentDto;
import com.pd.springclouddubboapi.service.StudentQueryServiceI;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.shenyu.client.dubbo.common.annotation.ShenyuDubboClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * TODO
 *
 * @PACKAGE_NAME: com.pd.springclouddubboprovider.controller
 * @AUTH: chendengqian
 * @DATETIME: 2022/12/1 20:59
 * @PROJECT_NAME: spring-cloud-dubbo-202104
 **/
@DubboService(group = "mc",version = "1.0.3")
@Slf4j
@RefreshScope
public class ShenyuStudentController implements StudentQueryServiceI {
    @Value("${stu}")
    private String name;
    private String age;

    @Override
    @ShenyuDubboClient("/queryStudent")
    public StudentDto queryStudent(StudentReq studentReq) {
        log.info("[请求参数:" + studentReq);
        StudentDto studentDto = new StudentDto();
        studentDto.setAddr("上海市浦东新区");
        studentDto.setName(name);
        studentDto.setAge(18);
        return studentDto;
    }
    @ShenyuDubboClient("/sayHello")
    @Override
    public void sayHello(){
        log.info("hello");
    }
}
