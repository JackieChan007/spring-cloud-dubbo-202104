package com.pd.springclouddubboshenyuprovider;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;

@SpringBootApplication
@Slf4j
public class SpringCloudDubboShenyuProviderApplication implements ApplicationListener<ApplicationStartedEvent> {

    public static void main(String[] args) {
        SpringApplication.run(com.pd.springclouddubboshenyuprovider.SpringCloudDubboShenyuProviderApplication.class, args);
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        log.info("--------------------------------------------------------");
        log.info("SpringCloudDubboShenyuProviderApplication started successfully");
        log.info("--------------------------------------------------------");
    }
}
