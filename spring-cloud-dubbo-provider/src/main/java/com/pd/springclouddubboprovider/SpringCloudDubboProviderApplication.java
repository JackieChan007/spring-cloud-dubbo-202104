package com.pd.springclouddubboprovider;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.context.ApplicationExt;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

@SpringBootApplication
@Slf4j
public class SpringCloudDubboProviderApplication implements ApplicationListener<ApplicationStartedEvent> {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudDubboProviderApplication.class, args);
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        log.info("--------------------------------------------------------");
        log.info("SpringCloudDubboProviderApplication started successfully");
        log.info("--------------------------------------------------------");
    }
}
