# 工程简介

## 说明

本项目采用如下依赖：

```xml

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-dependencies</artifactId>
    <version>${spring-boot.version}</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-alibaba-dependencies</artifactId>
    <version>${spring-cloud-alibaba.version}</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
```

属性定义：

```xml
<spring-boot.version>2.6.11</spring-boot.version>
<spring-cloud-alibaba.version>2021.0.4.0</spring-cloud-alibaba.version>
```

dubbo版本：

```xml
<dependency>
    <groupId>org.apache.dubbo</groupId>
    <artifactId>dubbo-spring-boot-starter</artifactId>
    <version>3.1.3</version>
</dependency>
```
nacos-server启动：
```shell
docker run --restart=always --name  nacos-quick -e MODE=standalone -p 8848:8848 -p 9848:9848 -d nacos/nacos-server:2.1.2
```

## 结论
- 1.本项目的主要作用是测试nacos+dubbo3兼容性和相关功能，作为生产实践的技术调研。
- 2.可以实现dubbo的调用，兼容性初步测试没问题。
- 3.可以实现springcloud注解多nacos的兼容，可以实现热刷新。

