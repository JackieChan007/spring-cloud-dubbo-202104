package com.pd.springclouddubbo202101shenyuprovider;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;

@SpringBootApplication
@Slf4j
public class SpringCloudDubbo202101ShenyuProviderApplication implements ApplicationListener<ApplicationStartedEvent> {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudDubbo202101ShenyuProviderApplication.class, args);
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        log.info("--------------------------------------------------------");
        log.info("SpringCloudDubboShenyuProviderApplication started successfully");
        log.info("--------------------------------------------------------");
    }
}
